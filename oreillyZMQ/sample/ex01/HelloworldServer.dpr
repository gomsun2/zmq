﻿program HelloworldServer;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  ZMQ.API

  , System.SysUtils
  ;

var LRequest: zmq_msg_t;
var LReply: zmq_msg_t;

begin
  try
    var LContext: Pointer := zmq_ctx_new;
    // Socket to talk to clients
    var LResponder := zmq_socket(LContext, ZMQ_REP);
    zmq_bind(LResponder,'tcp://*:5555');
    while True do
    begin
      zmq_msg_init(@LRequest);
      zmq_msg_recv(@LRequest, LResponder, 0);
      WriteLn('Receive Hello');
      zmq_close(@LRequest);

      // Do some work
      Sleep(1);

      // Send reply back to client
      var LBuf := TEncoding.ANSI.GetBytes('World');
      zmq_msg_init_size(@LReply, Length(LBuf));
      Move(LBuf[0], LReply, Length(LBuf));
      zmq_msg_send(@LReply, LResponder, 0);
      zmq_msg_close(@LReply);
    end;
    //  We never get here but if we did, this would be how we end
    zmq_close(LResponder);
    zmq_ctx_term(LContext);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
