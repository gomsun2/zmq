﻿program HelloworldClient;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  ZMQ.API

  , System.SysUtils
  ;

var LRequest: zmq_msg_t;
var LReply: zmq_msg_t;

begin
  try
    var LContext := zmq_ctx_new;
    WriteLn('Connecting to hello world server...');
    var LRequester: Pointer := zmq_socket(LContext, ZMQ_REQ);
    zmq_connect(LRequester, 'tcp://localhost:5555');
    for var i := 0 to 4 do
    begin
      var LBuf: TBytes := TEncoding.ANSI.GetBytes('Hello');
      zmq_msg_init_size(@LRequest, Length(LBuf));
      Move(LBuf[0], LRequest, Length(LBuf));
      WriteLn('Sending Hello ', i);
      zmq_msg_send(@LRequest, LRequester, 0);
      zmq_msg_close(@LRequest);

      zmq_msg_init(@LReply);
      zmq_msg_recv(@LReply, LRequester, 0);
      WriteLn('Received World ', i);
      zmq_msg_close(@LReply);
    end;
    Sleep(2);
    zmq_close(LRequester);
    zmq_ctx_term(LContext);
    Readln;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
